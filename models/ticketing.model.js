const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ticketinSchema = new Schema({
  author_id: {
    type: String,
    require: true
  },
  judul: {
    type: String,
    required: true
  },

  kategori: {
    type: String,
    required: true
  },

  lokasi: {
    type: String,
    required: true
  },

  isipengaduan: {
    type: String,
    required: true
  },

  foto: {
    type: String,
    required: true
  }
});

mongoose.model('ticketing', ticketinSchema);